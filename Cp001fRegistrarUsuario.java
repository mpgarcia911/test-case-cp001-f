package io.testproject.generated.tests.activetime;

import io.testproject.addon.WebExtensions;
import io.testproject.sdk.drivers.ReportingDriver;
import io.testproject.sdk.drivers.web.RemoteWebDriver;
import io.testproject.sdk.interfaces.junit5.ExceptionsReporter;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * This class was automatically generated by TestProject
 * Project: ActiveTime
 * Test: CP001-F - Registrar usuario
 * Generated by: Paula Garcia (mpgarcia91@ucatolica.edu.co)
 * Generated on Sun Oct 10 00:44:02 GMT 2021.
 */
@DisplayName("CP001-F - Registrar usuario")
public class Cp001fRegistrarUsuario implements ExceptionsReporter {
  public static WebDriver driver;

  @BeforeAll
  static void setup() throws Exception {
    driver = new RemoteWebDriver("qn0kC3Ce3bugGOycd7LAjpmtaQEb4U5FpkDF6OvO1DI", new ChromeOptions(), "Paula Garcia");
  }

  /**
   * In order to upload the test to TestProject need to un-comment @ArgumentsSource and set in comment the @MethodSource
   * @org.junit.jupiter.params.provider.ArgumentsSource(io.testproject.sdk.interfaces.parameterization.TestProjectParameterizer.class) */
  @DisplayName("CP001-F - Registrar usuario")
  @ParameterizedTest
  @MethodSource("provideParameters")
  void execute(String ApplicationURL, String genero, String name, String email, String email1)
      throws Exception {
    // set timeout for driver actions (similar to step timeout)
    driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
    By by;
    boolean booleanResult;
    WebExtensions.SelectOptionByValue selectOptionByValue;

    // 1. Navigate to '{{ApplicationURL}}'
    //    Navigates the specified URL (Auto-generated)
    GeneratedUtils.sleep(500);
    driver.navigate().to(ApplicationURL);

    // 2. Click 'Sign up'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#button-Register");
    driver.findElement(by).click();

    // 3. Click 'genero'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#genero");
    driver.findElement(by).click();

    // 4. Select all '{{genero}}' options in 'genero'
    GeneratedUtils.sleep(500);
    selectOptionByValue = WebExtensions.selectOptionByValue(genero);
    by = By.cssSelector("#genero");
    selectOptionByValue = (WebExtensions.SelectOptionByValue)((ReportingDriver)driver).addons().execute(selectOptionByValue, by, -1);

    // 5. Click 'genero'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#genero");
    driver.findElement(by).click();

    // 6. Click 'Next'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#next-genero");
    driver.findElement(by).click();

    // 7. Click 'name'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#userName");
    driver.findElement(by).click();

    // 8. Type '{{name}}' in 'name'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#userName");
    driver.findElement(by).sendKeys(name);

    // 9. Click 'Next1'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#next-usuario");
    driver.findElement(by).click();

    // 10. Click 'email'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#email");
    driver.findElement(by).click();

    // 11. Type '{{email}}' in 'email'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#email");
    driver.findElement(by).sendKeys(email);

    // 12. Click 'Email1'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#emailConfirmation");
    driver.findElement(by).click();

    // 13. Type '{{email1}}' in 'Email1'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#emailConfirmation");
    driver.findElement(by).sendKeys(email1);

    // 14. Click 'Next2'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#next-email");
    driver.findElement(by).click();

    // 15. Click 'Cancel'
    GeneratedUtils.sleep(500);
    by = By.cssSelector("#cancel-email");
    driver.findElement(by).click();

  }

  @Override
  public ReportingDriver getDriver() {
    return (ReportingDriver) driver;
  }

  @AfterAll
  static void tearDown() {
    if (driver != null) {
      driver.quit();
    }
  }

  private static Stream provideParameters() throws Exception {
    return Stream.of(Arguments.of("http://localhost/xampp/ActiveTime/index.html","","","",""));
  }
}
