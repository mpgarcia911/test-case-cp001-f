using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using System.Threading;
using System;
using TestProject.SDK;
using TestProject.SDK.Tests.Helpers;
using TestProject.SDK.Tests;
using TestProject.Common.Attributes;
using TestProject.Common.Enums;

namespace TestProject.Generated.Tests.ActiveTime
{
	/// <summary>
	/// This class was automatically generated by TestProject
	/// Project: ActiveTime
	/// Test: CP001-F - Registrar usuario
	/// Generated by: Paula Garcia (mpgarcia91@ucatolica.edu.co)
	/// Generated on: 10/10/2021 00:44:13
	/// </summary>
	public class CP001FRegistrarUsuario : IWebTest
	{
		[ParameterAttribute(Description = "Auto generated application URL parameter", DefaultValue = "http://localhost/xampp/ActiveTime/index.html", Direction = ParameterDirection.Input)]
		public string ApplicationURL;
		[ParameterAttribute(Direction = ParameterDirection.Input)]
		public string genero;
		[ParameterAttribute(Direction = ParameterDirection.Input)]
		public string name;
		[ParameterAttribute(Direction = ParameterDirection.Input)]
		public string email;
		[ParameterAttribute(Direction = ParameterDirection.Input)]
		public string email1;
		public ExecutionResult Execute(WebTestHelper helper)
		{
			var driver = helper.Driver;
			var report = helper.Reporter;
			bool boolResult;
			By by;
			ExecutionResult executionResult;
			io.testproject.addons.web.element.select.SelectOptionByValue selectAllOptionsByValueAttribute;
			
			// set timeout for driver actions (similar to step timeout)
			driver.Timeout = 15000;
			
			 // 1. Navigate to '{{ApplicationURL}}'
			// Navigates the specified URL (Auto-generated)
			// Add step sleep time (Before)
			Thread.Sleep(500);
			boolResult = driver.TestProject().NavigateToURL(ApplicationURL);
			report.Step(string.Format("Navigate to '{0}'", ApplicationURL), boolResult, TakeScreenshotConditionType.Failure);
			
			 // 2. Click 'Sign up'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#button-Register");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Sign up'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 3. Click 'genero'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#genero");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'genero'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 4. Select all '{{genero}}' options in 'genero'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#genero");
			selectAllOptionsByValueAttribute = TestProject.Addons.Proxy.WebExtensions.CreateSelectOptionByValue(genero);
			executionResult = helper.ExecuteProxy(selectAllOptionsByValueAttribute, by);
			report.Step(string.Format("Select all '{0}' options in 'genero'", genero), executionResult == ExecutionResult.Passed, TakeScreenshotConditionType.Failure);
			
			 // 5. Click 'genero'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#genero");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'genero'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 6. Click 'Next'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#next-genero");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Next'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 7. Click 'name'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#userName");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'name'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 8. Type '{{name}}' in 'name'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#userName");
			boolResult = driver.TestProject().TypeText(by, name);
			report.Step(string.Format("Type '{0}' in 'name'", name), boolResult, TakeScreenshotConditionType.Failure);
			
			 // 9. Click 'Next1'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#next-usuario");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Next1'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 10. Click 'email'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#email");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'email'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 11. Type '{{email}}' in 'email'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#email");
			boolResult = driver.TestProject().TypeText(by, email);
			report.Step(string.Format("Type '{0}' in 'email'", email), boolResult, TakeScreenshotConditionType.Failure);
			
			 // 12. Click 'Email1'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#emailConfirmation");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Email1'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 13. Type '{{email1}}' in 'Email1'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#emailConfirmation");
			boolResult = driver.TestProject().TypeText(by, email1);
			report.Step(string.Format("Type '{0}' in 'Email1'", email1), boolResult, TakeScreenshotConditionType.Failure);
			
			 // 14. Click 'Next2'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#next-email");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Next2'", boolResult, TakeScreenshotConditionType.Failure);
			
			 // 15. Click 'Cancel'
			// Add step sleep time (Before)
			Thread.Sleep(500);
			by = By.CssSelector("#cancel-email");
			boolResult = driver.TestProject().Click(by);
			report.Step("Click 'Cancel'", boolResult, TakeScreenshotConditionType.Failure);
			
			return ExecutionResult.Passed;
		}
	}
}